<?php

use yii\db\Migration;

/**
 * Class m180624_192032_create_user_admin
 */
class m180624_192032_create_user_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('users',[
            'name' => 'admin',
            'login' => 'admin',
            'password' => 'admin'
            ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_192032_create_user_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_192032_create_user_admin cannot be reverted.\n";

        return false;
    }
    */
}
