<?php

use yii\db\Migration;

/**
 * Class m180624_174915_create_table_project
 */
class m180624_174915_create_table_project extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(100),
            'name' => $this->string(255),
            'price' => $this->string(100),
            'date_created' => $this->string(),
            'date_completed' => $this->string(),

        ]);

        $this->createIndex(
            'idx-project-user_id',
            'project',
            'user_id'
        );

        $this->addForeignKey(
            'fk-project-user_id',
            'project',
            'user_id',
            'users',
            'id'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_174915_create_table_project cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_174915_create_table_project cannot be reverted.\n";

        return false;
    }
    */
}
