<?php

use yii\db\Migration;

/**
 * Class m180624_174348_create_table_user
 */
class m180624_174348_create_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'login' => $this->string(255),
            'password' => $this->string(100),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_174348_create_table_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_174348_create_table_user cannot be reverted.\n";

        return false;
    }
    */
}
